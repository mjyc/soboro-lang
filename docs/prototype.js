// ---------------------------------
// Core Utility Function Definitions
// ---------------------------------

const makeActionArgDecorator = (decorate) => {
  return (actionArg) => {
    return {
      input: decorate.input(actionArg.input),
      transition: decorate.transition(actionArg.transition),
      output: decorate.output(actionArg.output),
    };
  };
};

const makeAction = (actionArg) => {
  return (sources) => {
    const input$ = actionArg.input(sources);
    const state$ = state(input$);
    const outputs = output(state$);
    return Object.assign(
      {
        state: state$,
      },
      outputs
    );
  };
};

// -------------
// Example Usage
// -------------

const dec1 = makeActionArgDecorator({});
const dec2 = makeActionArgDecorator({});

const Action1 = makeAction(dec1({}));
const Action2 = makeAction(dec2({}));
const Action3 = makeAction(dec1(dec2({})));

// ---------------------------------
// Core Utility Function Definitions
// ---------------------------------

const makeComponent = (arg) => {
  return (sources) => {
    const input$ = arg.input(sources);
    const transition$ = input$.map(arg.transition);
    const outputs = arg.outputs(sources);
    return Object.assign(
      {
        state: reducer$,
      },
      outputs
    );
  };
};

const makeAction = ({}) => {
  return makeComponent();
};

const Say = ({ name, text, startOn }) => {};

const Decorator = ({}) => {};

const Sequence = ({}) => {
  const dec1 = makeActionArgDecorator({});
  const dec2 = makeActionArgDecorator({});

  return;
};
