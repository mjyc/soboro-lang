# User Research

## Target Users

1. non-developers like teachers, parents, caregivers, etc.
1. web developers
1. ROS users
1. research collaborators

## Target Robot Platforms

- tablet robot face
- EMAR robots
- Openmanipulator & Kuri
- Alexa & Google home
- Microcontrollers

## Target Software Frameworks

- Reactive programming framework, e.g., Cycle.js, RXJS, etc.
- ROS
- CircuitPython

## User Requirements

1. support both imperative programming and reactive programming paradigms
1. ease of programming and debugging
1. ease of deployment
