# Ideas

## ActionScript RoboScript Whatever

### Patterns

Based on sauppe2014design

- Question and Answer
  - ask question and waiting for an answer
  - support multiple question type (as in google form, dialogue flow, SuperScript/RiveScript)
  - warning: don't try to make another ChatScript

- Instruction and Action
  - wait for a particular event; otherwise...
  - many different ways to measure, right?

- Monologue
  - timeout
  - animations
  - handling comment

- Listening (backchanneling)

### DSL

Based on chatscript

- basics
  - theme (topic)
  - robot action (gambit)
  - human trigger (response)

- new
  - patterns for identifying (disc) event
    - desired ones: sequence, boundaries (< only), wild card (\*n, ... ), unordered Matching, ...
    - idea: just take a very few of them for my demo
    - idea: find matching FRP operators
  - cont in/out-puts
    - disc -> disc is there
    - disc -> cont (using spring)
    - cont -> disc (thresholding, new syntax)
    - cont -> cont (continuous math, new syntax)
  - bulk editing or css
    - adding actions (see the `~` in CS)
    - controlling timings (thought and action paradigm)
    - showing intentions (HRI papers)

## RBML

- explore exploiting the action properties, e.g., state vs. event (for automatically connecting sensing and acting) and durative vs. instantaneous (for better supporting imperative programming)
  - a framework for (automatically) creating controllers
    - (sensing) discrete-to-discrete (actuating)
    - (sensing) discrete-to-continuous (actuating)
    - (sensing) continuous-to-discrete (actuating)
    - (sensing) continuous-to-continuous (actuating)
  - automatically detecting decorator vs. action via type, e.g., durative vs. instantanous

- a framework for (automatically) converting signals
  - discrete-to-discrete, e.g., a state transition rule
  - discrete-to-continuous, e.g., react-spring
  - continuous-to-discrete, e.g., a state transition rule
  - continuous-to-continuous, e.g., controller

- a framework for storing, reading, visualizing, and acting to data, get ideas from
  - rosbag/rviz and web integration, e.g., webviz,
  - google forms

- usability test
  - start testing with the bottom-most to the top-most target users
  - think about what to look for during the tests, e.g.,
    - what are the major factors for (not) using soboro?

- bulk editing
  - gitlab board editing
  - spreadsheet font editing

- data collection integration
