# Prototype DSL

## Idea 8

To DOs

- finish the example #2
- create examples for instructions and q&a tasks
- define behaviors for composing actions
- concretize stream variable interpretation
- concretize using "needs", "gives" for expressing dependencies (for actuators and sensors)

```html
<!-- Example #1 -->

<!-- Sequence -->
<Sequence>
  <Say text="hello">
  <Say text="nice to meet you">
  <Say text="bye">
</Sequence>

<!-- which equals to -->
<Decorator startWhen="Say.result.index == index - 1}">
  <Decorator index=0 startWhen="programStart"> <!-- "startWhen" is overwrites the default -->
    <Say text="hello">
  </Decorator>
  <Decorator index=1>
    <Say text="nice to meet you">
  </Decorator>
  <Decorator index=2>
    <Say text="bye">
  </Decorator>
</Decorator>


<!-- Example #2 -->

<!-- Sequence with non-default args-->
<Sequence
  startWhen="!isHumanSpeaking"
  pauseWhen="!faceVisible"
  resumeWhen="faceVisible"
  resumeTo="current"
>
  <Say text="hello">
  <Say text="nice to meet you">
  <Say text="bye">
</Sequence>

<!-- which also equals to -->
<Decorator startWhen="Say.result.index == index - 1">
  <Decorator index=0 startWhen="programStart"> <!-- "startWhen" is overwrites the default -->
    <Say text="hello">
  </Decorator>
  <Decorator index=1>
    <Say text="nice to meet you">
  </Decorator>
  <Decorator index=2>
    <Say text="bye">
  </Decorator>
</Decorator>
<Decorator
  startWhen="isSaying.index == index && !isFaceVisible"
  endWhen="isSaying.text == '' && isFaceVisible"
  next="index"
>
  <Say text=""/>
  <Say text=""/>
  <Say text=""/>
</Decorator>
```


## Idea 7

Monologue

```html
<!-- Sequence -->
<!-- "Sequence" is a template-->
<Sequence>
  <Say text="hello">
  <Say text="nice to meet you">
  <Say text="bye">
</Sequence>

<!-- "Sequence" compiles to this -->
<!-- "Decorator" make and apply an action arg decorator -->
<Decorator
  startOn="programStart"
  endOn="sayResult.name == 'say1'"
>
  <!-- Say also is a template for "Action" -->
  <Say name="say1" text="hello">
</Decorator>
<Decorator
  startOn="sayResult.name == 'say1'"
  endOn="sayResult.name == 'say2'"
>
  <Say name="say2" text="nice to meet you">
</Decorator>
<!-- ... -->


<!-- Sequence Example #1 -->
<!-- resume="previous"/"current"/"next"/{id}} -->
<Sequence
  nextOn="!isHumanSpeaking"
  pauseOn="!isFaceVisible",
  resumeOn="isFaceVisible",
  resume="current",
>
  <Say text="hello">
  <Say text="nice to meet you">
  <Say text="bye">
</Sequence>

<!-- "Sequence #1" compiles to this -->
<!-- "Pause"s -->
<Decorator
  startOn="!isFaceVisible && isSaying.name == 'say1'"
  endOn="sayResult.name == 'sayPause' && isFaceVisible"
  next="say1"
>
  <Say name="sayPause" text="bye">
</Decorator>
<Decorator
  startOn="!isFaceVisible && isSaying.name == 'say2'"
  endOn="sayResult.name == 'sayPause' && isFaceVisible"
  next="say2"
>
  <Say name="sayPause" text="bye">
</Decorator>

<!-- "Say"s -->
<Decorator
  startOn="programStart"
  endOn="sayResult.name == 'say1' && !isHumanSpeaking"
>
  <Say name="say1" text="hello">
</Decorator>
<Decorator
  startOn="sayResult.name == 'say1'"
  endOn="sayResult.name == 'say2' && !isHumanSpeaking"
>
  <Say name="say2" text="nice to meet you">
</Decorator>
<!-- ... -->


<!-- Sequence + Decorators Design #1 -->
<Sequence>
  <Say text="hello">
  <Decorator output="setColorToRed">
    <Decorator output="followFace">
      <Say text="nice to meet you">
    </Decorator>
  </Decorator>
  <Say text="bye">
</Sequence>

<!-- Sequence + Decorators Design #2 -->
<Sequence>
  <Decorator match="face" output="followFace"/>
  <!-- <Decorator match="red" output="setColorToRed"/> -->
  <Rule match="red" output="setColorToRed"/>
  <Say text="hello">
  <Say text="nice to meet you" decorators="face red">
  <Say text="bye">
</Sequence>

<!-- Sequence + Decorators Design #3 -->
<Decorator name="face" output="followFace"/>
<Decorator name="red" output="setColorToRed"/>
<Sequence decorators="face red">
  <Say text="hello">
  <Say text="nice to meet you">
  <Say text="bye">
</Sequence>
```

Neck Exercise

```html
```

Q&A

```html
```


## Idea 6

Monologue

```
[Sequence]
  [Pause when="faceIsNotVisible"]
    [Say text=""]
  [/Pause]
  [Resume when="faceVisible"]
  [/Resume]
  [Next when="faceIsVisible && silent"]
  [Actions]
    [Say text="hello"]
    [Say text="nice to meet you"]
    [Say text="bye"]
  [/]
[/Sequence]
```

```
[Sequence]
  [Pause when="faceIsNotVisible"]
  [Resume when="faceVisible" run="current/next/prev/{id}"]
  [Step when="faceIsVisible && silent"]
  [Say text="hello"]
  [Say text="nice to meet you"]
  [Say text="bye"]
[/Sequence]
```

```
[Sequence]
  [Pause when="faceIsNotVisible"]
  [Resume when="faceVisible" action="paused/next/prev/{id}"]
  [Step when="faceIsVisible && silent"]
  [Say text="hello"]
  [Say text="nice to meet you"]
  [Say text="bye"]
[/Sequence]
```

```
[Pause when="faceIsNotVisible" do="stopSay"]
[Resume when="faceVisible"]
[FollowFace do="followFace"]
[StopFollowFace do="followFace2"]
[Sequence attr=[Pause, Resume, FollowFace, StopFollowFace]]
  [Say1 text=""]
  [Say2 text="" attr="ff2"]
  [Say3 text=""]
[/Sequence]
```

```json
{

}
```

Instruction

```
[Sequence]
  [Pause when="faceIsNotVisible"]
  [Resume when="faceVisible" action="paused/next/prev/{id}"]
  [Step when="faceIsVisible && silent"]

  [Say text="hello"]
  [Say text="nice to meet you"]
  [Say text="bye"]
[/Sequence]
```


```
[Attr name="ff1" do="followFace"]
[Attr name="ff2" do="followFace2"]

[Sequence]
  [Pause when="faceIsNotVisible" do="stopSay"]
  [Resume when="faceVisible"]
  [Step
    when="say.lastResult.text==='turn your head to left' && instrunctionDone"
    attr="ff1"
  ]
  [Instruct1 name="inst1" text="turn your head to left"]
  [Instruct1 name="inst1" text="turn your head to right"]
  [Instruct1 name="inst1" text="turn your head to left"]
  [Instruct1 name="inst1" text="turn your head to right"]
[/Sequence]
```

Q&A

[DefaultAsk when="faceIsVisible && silent" attr="ff1"]

```
[Behavior]
  [Pause when="faceIsNotVisible" do="stopSay"]
  [Resume when="faceVisible"]
  [Sequence defaultNextWhen="faceIsVisible && silent" defaultAttr="ff1"]
    [Ask name="ask1" next="val == 'yes' ? 'ask2' : 'ask3'"]
    [Ask name="ask2" next=""]
    [Ask name="ask3" next=""]
  [/Sequence]
[/Behavior]
```

v1

```
{
  type: "sequence"
  pause: {
    when: "faceIsNotVisible"
    do: "stopSay"
  },
  resume: {

  },
  defaultStep: {
    attrs: []
  },
  steps: [
    {
      type: "Ask",
      name: "q1",
      text: "",
      next: "result == 'yes' ? 'ask2' : 'ask3'"
    },
    {type: "Ask", name: "q2", text: "", next: ""},
  ]
}
```

v2

```
{
  "pause": {
      "when": "!isFaceVisible"
      "do": "stopSay"
    },
    "resume": {
      "when": "isFaceVisible"
      "do": "stopSay"
    },
}
```

```
{
  attrs: {
    "next": "",
    "pause": {
      "when": "!isFaceVisible"
      "do": "stop"
    },
    "resume": {
      "when": "isFaceVisible"
      "do": "stopSay"
    },

  },
  "default": {


  },
  "sequence": [
    {
      "type": "Ask",
      "id": "q1",
      "text": "What's your name?"
      "next": "result == 'yes' ? 'q2' : 'q3'"
    }
  ]
}
```


## Idea 5

Monologue

```
[Behavior]
  [Rule] _Speak iff human is not speaking_ [/Rule]
  // combine(faceVisible, start)
  [Rule] _Stop if the human is not visible_ [/Rule]
  // combine(faceNotVisible, stop)
  [Rule] _Resume if the human is visible again_ [/Rule]
  // combine(faceVisible, resume)
  [Rule] _Move to the next sentence if the human is visible again_ [/Rule]
  // combine(faceVisible, next)
  // gotoX
  [Do][/Do]
  [Do][/Do]
  [Do][/Do]
[/Behavior]
```

Q&A

```
[Behavior]
  [Rule] _Speak iff human is not speaking_ [/Rule]
  [Rule] _Stop if the human is not visible_ [/Rule]
  [Rule] _Resume if the human is visible again_ [/Rule]
  [Rule] _Resume if the human is visible again_ [/Rule]
  [Do][/Do]
  [Do][/Do]
  [Do][/Do]
[/Behavior]
```


## Idea 4

```
{
  type: "monologue",
  transitions: {
    nextOn: {},
    pauseOn: {},
    whenPausedDo: {},
    prevOn: {},
    whenPrevedDo: {},
  },
  do: [
    {say: "hello"},
    {say: "hello"},
    {say: "hello"},
  ]
}
```

```
[Monologue
  wrap=
]
  [Attr
    name="say0"
    when="faceDisappeared & sayDone"
    do="say 'are you still here?'"
  /]
  [Attr
    name="say0"
    when="faceReappeared"
    do="repeatPrevious" or "sayNext"
  /]

  [Say
    name="say0"
    when="programStarted"
    do="say Hello"
  /]
  [Say
    name="say0"
    when=""
    do="say Hello"
  /]
[Monologue/]

```

```js
// define attributes & bases
var attrs = [{
  type: "attr",
  input: {
    calculate: "combine(sources.sinkName1, parentSink.sinkName2)"
  },
  model: {

  },
  output: {
    calculate: {}
  }
  name: ""
},];

var actions = [
  {
    triggers: [
      // {} // dependent on sources
      // if drop(1, combine(a, b)) then b
    ],
    output: {
      "say": "Hello world!"
    },
    attrs: ["attr1", "attr2"]
  }
];
```


## Idea 3

```js
// define attributes & bases
var attrs = [{
  type: "attr",
  input: {
    calculate: "combine(sources.sinkName1, parentSink.sinkName2)"
  },
  model: {

  },
  output: {
    calculate: {}
  }
  name: ""
},];
```


## Idea 2

```js
// define attributes & bases
{
  type: "decorator", // call it something different
  args: {
    input: (sources, parentSink) => {},
    state: () => {},
    output: () => {},
  },
  name: ""
},
{

}
```


## Idea 1

```html
<makeDecorator
  name="decorator1" // variable name
  input={{}} //
  state={{}} //
  output={{}} //
/>

<actionArgs
  name="action1" // variable name
  input={{}} //
  state={{}} //
  output={{}} //
/>

<decorate
  action="action1",
  decorators=["decorator1", "decorator2", ...]
/>

<makeAction
  action=action1
/>
```
