# Prototype Design

## Target Tasks

1. Storytelling
  - read story line-by-line
  - handle arrival and departure
  - engagement management, e.g., handles interruption and temporal disengagement

2. Neck exercise
  - instruct one-by-one
  - monitor the user and give feedback

3. Question and answer
  - asks questions one-by-one
  - flowchart-like navigation


## Idea 3

## The Problems

- Creating FSM is surprising difficult

## Approaches

- macros and templates
- action compositions
- smart defaults
- WYSIWYG or program visualization


## Idea 2

### The Problem

Creating FSM is surprising difficult

### High-Level Approaches

- structure enforcement
- macros and templates
- WYSIWYG; dataviz
- smart defaults
- (optional) data collection

#### Details

- makes creating SimpleActionServer components easy

### Basic Concepts

- Driver
  - a function makes side effects
  - communicates with hardwares
  - (WIP) e.g., SpeechSynthesisAction, PoseDetector, TabletFaceAction, etc.

- Component
  - a function that takes stream inputs and returns stream outputs

- Action
  - is a type of component or driver that
  - takes "goal" and "cancel" streams and returns "result" and "status" streams and implements a ROS' SimpleActionServer like interface
  - additionally takes input streams from drivers or other components and returns output streams for drivers or other components

### Structure

- Drivers should interact with underlying devices (or device interfaces) independently
  - there shouldn't be SpeechSynthesisA and SpeechSynthesisB that both interface with the same SpeechSynthesis device
- (WIP) _Automatically creates components that behave like FSMs_


## Idea 1

- description
  - start event
  - running event
  - end event

"storytelling"

1. equality:

```
[Say data="hello" startwhen=programStart endwhen=sayDone /]
; equals [Say id="hello" data="hello" start="programStart == true" end="sayResult.id == 'hello'" /]
```

2. sequence:

```
[Say data="hello" startwhen=programStart endwhen=sayDone /]
[Say data="nice to meet you" startwhen="sayDone.data == 'hello'" endwhen="sayDone.data == 'nice to meet you'" /]
[Say data="bye" startwhen="sayDone.data == 'nice to meet you" endwhen="sayDone.data == 'bye'" /]
```

3. handling arrival & departure; v1 - interrupt what it was doing and repeat:

```
[Say data="hello" startwhen=programStart endwhen=sayDone /]
[Say data="nice to meet you" startwhen="sayDone.data == 'hello' || sayDone.id == 'sayDoneInterrupt'" endwhen="sayDone.data == 'nice to meet you'" /]
[Say data="bye" startwhen="sayDone.data == 'nice to meet you' ... " endwhen="sayDone.data == 'bye'" /]

[Say data="where are you going?" startwhen="personLeft && sayDone == curSay" endwhen=sayDone /]
[Say data="nice come back" startwhen=personLeft endwhen=sayDone /]
```

### Composite Action Ideas

1. template

```
[Monologue
  data=[...]
  startwhen=programStart
  endwhen=lastSayDone
  transition=sayDone && humanQuite
]
  [Say data="where are you going?" startwhen="personLeft && sayDone == curSay" endwhen=sayDone /]
  [Say data="nice come back" startwhen=personLeft endwhen=sayDone /]
[/Monologue]
```

2. parallel

3. continuous <-> discrete

### Design Rationale

Action
- why is the action the basic unit of behaviors?
- preemptive RPC & durative action
  - because that is how ROS does it, i.e., it is a popular definition/understanding of "action" (implementation)
- "complex event" as a trigger condition
  - allows creating complex behaviors
  - distinguish itself from Behavior Tree

Timing
- soboro requires actions to provide timing, speed, trajectory parameter
- we want to free the users from thinking about durations and synchronization

Unorganized but feels important
- want to create expressive robot behaviors but behavior tree & FSMs were not enough
- reactive programming paradigm; implementing sequence was unnecessarily complex
- I wanted using a programming language to feel like writing texts

### Basic Concepts

- Action
  - takes goal & cancel

- Resource
  - inputs are streams & outputs are streams

- ...
