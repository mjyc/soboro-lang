# Week 19

Goals

- iterate the markup language
- update programs for the target applications

Updates

- started prototyping a new DSL
  - the [markup language](./prototype.html) or the [declarative specification](../examples/src/dsl/index.md#transpile) approach felt too complex.
  - the main benefit, e.g., templates & functional programming approach didn't feel too strong.
- reviewed related work
  - [ChatScript](https://github.com/ChatScript/ChatScript)
  - [reveal-md](https://github.com/webpro/reveal-md), i.e., markdown powered [reveal.js](https://github.com/hakimel/reveal.js), [mermaid](https://mermaidjs.github.io/), and [idyll](http://idyll-lang.org/)
  - vega-lite
    - provides a declarative way to create charts by commonly used templates & attributes.
    - our work is about creating something, rather than processing something.
  - Exploring relationships between interaction attributes and experience by Lenz et al
    - focuses on "why" and "how" for interaction design.
    - "why" is what people want but most PL or DSL allow users to control "how" because "why" is hard, subjective, etc.
  - Design Patterns for Exploring and Prototyping Human-Robot Interactions by Sauppe et al & Recognizing Engagement in Human-Robot Interaction by Rich et al
    - provides HRI templates. Templates are not flexible; can custom template + crowdsourcing solve this problem? like React?
- started a new DSL, [soboro-script](https://gitlab.com/mjyc/soboro-script/-/tree/master/docs)
  - 👍 simplicity
  - 👎 difficult to integrate with a real programming language, it recreates a part of them



# Week 17

Progress updates

- Robot behavior specification language prototype for authoring target specification applications
  - e.g., storytelling (monologue), exercise coach (instruction), personality test (interview or flowchart)
  - demo [json](https://gitlab.com/mjyc/soboro-lang/-/blob/abe5660146d0967fe864e767a8e57741f82136e5/examples/src/dsl/index.test.js#L405-538), [html](https://gitlab.com/mjyc/soboro-lang/-/blob/abe5660146d0967fe864e767a8e57741f82136e5/docs/prototype.html)
  - [documentation](https://gitlab.com/mjyc/soboro-lang/-/blob/e41a22b7d930b294660f814319b744ceab720c07/examples/src/dsl/index.md#transpile)
  - major contributions
    - robot behavior authoring as editing, adding to, and ordering actions

Goals

- demo
  - target application authoring demo on a robot platform
  - program visualization, e.g., FSM or graph
  - continuous outputs
    - e.g., eye position control
    - idea: smooth outputs overtime using spring physics
    - related work: [rx-ease](https://github.com/gvergnaud/rx-ease)
- (stretch) multiple robot platforms
  - tabletrobot, cosmo, turtlebot, kuri, emar, etc.
- (stretch) rosette integration
  - demo verification, and synthesis (planning)
- (eventual) opensource

Contributions

- DSL that supports HRI pattern, HRI actions

Ideas

- data collection via event stream expression
- infobot tasks (check, find, monitor, summary) via event stream expression + planning (synthesis)

Chores

- update the storytelling (html & json) to demo supporting "constraints"
- update the storytelling (html & json) to demo using "continuous" outputs
- everything is a macro decorator
  - introduce "reduce"
  - update "concat" and "with" to a macro decorator with "reduce" field
- re-think about ordering
  - how can I support "randomize"?
  - e.g., sequence, flowchart, randomize
  - introduce "next"
- organization
  - pkgs
    - cyclejs-tabletface (or just add this in examples/cyclejs-tabletface)
    - rbsl
      - src
        - transpiler
        - compiler
        - interpreter
      - examples
        - cyclejs-tabletface
          - demo target tasks
          - demo data visualization, e.g., with vega-lite
        - rospy-turtlebot (or fetch, etc.)
          - demo mobile robot tasks
          - demo notification, recovery via bypasser (if no wifi) or operator, and take-over
        - python-cozmo
          - multi-object interaction


# Week 15

Explore

- action composition


# Week 13

Explore

- transition rule group update, e.g., via exploiting the HFSM structure
  - related work
    - spreadsheets
    - gitlab boards
- reactive states
  - continuous control
  - using meta data for sharing min/max frequency (look into that of callbag)
- (optional) DB integration


# Week 11

- [prototype design](./prototype_design.md)
- [user research](./user_research.md)
