#lang rosette/safe

(require
  rackunit rackunit/text-ui
  (prefix l/ "../lang.rkt")
  (prefix-in logger- "../logger.rkt")
  )

(provide (all-defined-out))

(define (test-1)
  (test-case
    "test-1"
    (check-equal? #t #t)
    )
  )

(define (test-2)

  ; define input
  ; define output
  ; define program
  (define program
    (list

      (l/component
        identity
        identity
        (list (l/component
          (lambda x frp/map)
          (lambda x frp/map)
          (lambda x frp/mapTo "hello")
          ))
        )

      (l/component
        (lambda x frp/map)
        (lambda x frp/map)
        (lambda x frp/mapTo "hello")
        )

      )
    )

  ; run against expected

  ; (define action1 (action
  ;   "say"
  ;   (lambda sources null) ; input
  ;   (lambda sources null) ; state
  ;   (lambda sources null) ; output
  ;   '(action2 ) ; children
  ;   ))

  ; (define action1 (action
  ;   "say"
  ;   (lambda sources null) ; input
  ;   (lambda sources null) ; state
  ;   (lambda sources null) ; output
  ;   '(action2 ) ; children
  ;   ))

  )


(module+ test
  (define/provide-test-suite lang-tests
    (test-1)
    )
  (run-tests lang-tests)
  )
