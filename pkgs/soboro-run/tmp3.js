const a = ```
<Decorator index=0 startWhen="programStart"> <!-- "startWhen" is overwrites the default -->
  <Say text="hello">
</Decorator>
<Decorator index=1>
  <Say text="nice to meet you">
</Decorator>
<Decorator index=2>
  <Say text="bye">
</Decorator>
```;

const rootChildren = [
  {
    type: "decorator",
  },
];

const interpreter = (node) => {
  if (node.type === "main") {
  }

  if (node.type === "action") {
    return {};
  }
  if (node.type === "decorator") {
    // create decorator and apply?
    return dec1(interpreter(node.child));
  }
};
