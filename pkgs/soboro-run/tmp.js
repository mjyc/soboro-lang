const main = sources => {
  return {
    SpeechSynthesis: {}
  };
};

const wrapper = main => {
  const sinks = main();

  return {
    ...main,
    SpeechSynthesis: xs.merge(
      sinks.SpeechSynthesis,
      sources.SpeechSynthesisAction.SpeechSynthesis
    ),
    SpeechSynthesisSource: sources.SpeechSynthesis
  };
};

// send a list of functions for creating drivers, e.g.,

// makeSpeechSynthesisDriver
// makeSpeechRecognitionDriver
// makeSpeechSynthesisActionDriver
// makeSpeechRecognitionActionDriver

const drivers = {
  SpeechSynthesis: makeSpeechSynthesisDriver(),
  SpeechRecognition: makeSpeechRecognitionDriver(),
  SpeechSynthesisAction: makeSpeechSynthesisActionDriver(),
  SpeechRecognitionAction: makeSpeechRecognitionActionDriver()
};

sink.actions.filter(x => x.type === "name");

// create a list of functions for creating wrappers

const makeSpeechRecognitionActionDriver = sinks => {
  // function input(
  //   goal$: Stream<Goal | string>,
  //   cancel$: Stream<GoalID>,
  //   startEvent$: Stream<any>,
  //   delayedendEvent$: Stream<any>
  // ) {}
  //
  sinks.actions.filter(x => x.type === "SpeechRecognitionAction");
  // sinks.states.filter(x => x.type === "SpeechSynthesis.startEvent")
  sinks.sensors.filter(x => x.type === "SpeechSynthesis.startEvent");

  sinks.SpeechSynthesisSource;
  return sources;
};

run();

// define sensors
fromEvent;

// define ... something
