const xs = require("xstream").default;
const { run } = require("@cycle/run");
const { div, button } = require("@cycle/dom");
const { runTabletFaceRobot } = require("soboro-robots");

const main = sources => {
  sources.SpeechSynthesisAction.status.addListener({ next: console.warn });
  sources.PoseDetection.filter(x => x.type === "PoseDetection/poses")
    .map(x => x.value)
    .addListener({ next: poses => console.log("poses", poses) });

  const input$ = sources.DOM.select(".start").events("click");
  const vdom$ = xs
    .combine(
      xs.of(button(".start", "start")),
      sources.PoseDetection.filter(x => x.type === "PoseDetection/dom").map(
        x => x.value
      )
    )
    .map(vdoms => div(vdoms));
  return {
    DOM: vdom$,
    SpeechSynthesisAction: input$.map(_ => ({ type: "goal", value: "hello" }))
  };
};

runTabletFaceRobot(main);
