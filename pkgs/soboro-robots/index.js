const xs = require("xstream").default;
const { run } = require("@cycle/run");
const { makeDOMDriver } = require("@cycle/dom");
const {
  makePoseDetectionDriver,
  makeSpeechSynthesisDriver,
  makeSpeechSynthesisActionDriver
} = require("soboro-drivers");

const runTabletFaceRobot = (main, drivers = {}) => {
  const defaultDrivers = {
    DOM: makeDOMDriver("#app"), // TODO: update to take the first one
    PoseDetection: makePoseDetectionDriver(),
    SpeechSynthesis: makeSpeechSynthesisDriver(),
    SpeechSynthesisAction: makeSpeechSynthesisActionDriver()
  };
  const withTabletFaceRobot = subMain => {
    return sources => {
      const subSinks = subMain(sources);
      const sinks = {
        DOM: subSinks.DOM,
        SpeechSynthesis: xs.merge(
          sources.SpeechSynthesisAction.SpeechSynthesis,
          subSinks.SpeechSynthesis || xs.never()
        ),
        SpeechSynthesisAction: xs.merge(
          sources.SpeechSynthesis,
          subSinks.SpeechSynthesisAction || xs.never()
        )
      };
      return sinks;
    };
  };
  run(withTabletFaceRobot(main), Object.assign({}, defaultDrivers, drivers));
};

module.exports = {
  runTabletFaceRobot
};
