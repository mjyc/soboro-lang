const xs = require("xstream").default;
const fromEvent = require("xstream/extra/fromEvent").default;
const {
  makePoseDetectionDriver: makePoseDetectionDriverOrig
} = require("cycle-posenet-driver");
const {
  makeSpeechSynthesisDriver: makeSpeechSynthesisDriverOrig,
  SpeechSynthesisActionFncs
} = require("@cycle-robot-drivers/speech");

const makePoseDetectionDriver = options => {
  const driver = makePoseDetectionDriverOrig(options);
  return sink$ => {
    const eventSource = driver(sink$);
    return fromEvent(window, "load")
      .map(_ =>
        xs.merge.apply(
          xs,
          ["dom", "poses"].map(name =>
            eventSource
              .events(name)
              .map(x => ({ type: `PoseDetection/${name}`, value: x }))
          )
        )
      )
      .flatten();
  };
};

const makeSpeechSynthesisDriver = () => {
  const driver = makeSpeechSynthesisDriverOrig();
  return sink$ => {
    const eventSource = driver(sink$);
    return xs.merge.apply(
      xs,
      ["start", "delayedend"].map(name =>
        eventSource
          .events(name)
          .map(x => ({ type: `SpeechSynthesis/${name}`, value: x }))
      )
    );
  };
};

const makeSpeechSynthesisActionDriver = () => {
  return sources => {
    const input$ = SpeechSynthesisActionFncs.input.apply(
      SpeechSynthesisActionFncs.input,
      [
        "goal",
        "cancel",
        "SpeechSynthesis/start",
        "SpeechSynthesis/delayedend"
      ].map(type =>
        sources.filter(x => x.type === type).map(({ value }) => value)
      )
    );
    const reducer$ = SpeechSynthesisActionFncs.transitionReducer(input$);
    const state$ = reducer$.fold((s, reducer) => reducer(s), {}).drop(1);
    const status$ = SpeechSynthesisActionFncs.status(state$);
    const outputs = SpeechSynthesisActionFncs.output(state$);
    return Object.assign({ status: status$ }, outputs);
  };
};

module.exports = {
  makePoseDetectionDriver,
  makeSpeechSynthesisDriver,
  makeSpeechSynthesisActionDriver
};
