const xs = require("xstream").default;
const { run } = require("@cycle/run");
const { makeDOMDriver, div, button } = require("@cycle/dom");
const {
  makePoseDetectionDriver,
  makeSpeechSynthesisDriver,
  makeSpeechSynthesisActionDriver
} = require("soboro-drivers");

const main = sources => {
  sources.SpeechSynthesisAction.status.addListener({ next: console.warn });
  sources.PoseDetection.filter(x => x.type === "PoseDetection/poses")
    .map(x => x.value)
    .addListener({ next: poses => console.log("poses", poses) });

  const input$ = sources.DOM.select(".start").events("click");
  const vdom$ = xs
    .combine(
      xs.of(button(".start", "start")),
      sources.PoseDetection.filter(x => x.type === "PoseDetection/dom").map(
        x => x.value
      )
    )
    .map(vdoms => div(vdoms));
  return {
    DOM: vdom$,
    SpeechSynthesis: sources.SpeechSynthesisAction.SpeechSynthesis,
    SpeechSynthesisAction: xs.merge(
      input$.map(_ => ({ type: "goal", value: "hello" })),
      sources.SpeechSynthesis
    )
  };
};

const drivers = {
  DOM: makeDOMDriver("#app"),
  PoseDetection: makePoseDetectionDriver(),
  SpeechSynthesis: makeSpeechSynthesisDriver(),
  SpeechSynthesisAction: makeSpeechSynthesisActionDriver()
};

run(main, drivers);
