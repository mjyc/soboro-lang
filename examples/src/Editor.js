const xs = require("xstream").default;
const { div, button, label, span, textarea } = require("@cycle/dom");

function intent(domSource) {
  return domSource
    .select("textarea.program-editor")
    .events("input")
    .map((ev) => ({
      type: "update_program",
      value: {
        type: ev.target.dataset.type,
        value: ev.target.value,
      },
    }));
}

function model(action$) {
  return action$.fold(
    (prev, input) => {
      if (input.type === "update_program")
        return { rawProgramTxt: input.value.value };
      return prev;
    },
    {
      rawProgramTxt: "",
    }
  );
}

function view(state$) {
  const styles = {
    body: {
      display: "flex",
      minHeight: "100vh",
      flexDirection: "column",
    },
    navbar: {
      height: "30px",
      background: "yellowgreen",
      display: "flex",
      alignItems: "center",
    },
    main: {
      flex: 1,
      background: "beige",
      padding: "5px",
      display: "flex",
      flexDirection: "row",
    },
    col: {
      flexGrow: 1,
      display: "flex",
      flexDirection: "column",
    },
    // col_label: {
    //   margin: "0px 0px 5px 0px",
    // },
    pane: {
      flex: 1,
      display: "flex",
    },
    pane_textarea: {
      resize: "none",
      flex: 1,
      margin: "0px 5px 5px 0px",
      fontFamily: "monospace",
    },
    navbar_span_btnGroup: {
      margin: "0 5px",
    },
  };
  return state$.map((state) =>
    div(".body", { style: styles.body }, [
      div(".navbar", { style: styles.navbar }, [
        span(".btn-group", { style: styles.navbar_span_btnGroup }, [
          label("go to "),
          button(".robot", "robot face"),
          button(".camera", "camera feed"),
        ]),
        span(".btn-group", { style: styles.navbar_span_btnGroup }, [
          label("do "),
          button(".run", "run"),
          button(".stop", "stop"),
        ]),
      ]),
      div(".main", { style: styles.main }, [
        div(".col", { style: styles.col }, [
          div(".pane", { style: styles.pane }, [
            textarea(
              ".program-editor",
              { style: styles.pane_textarea },
              {
                attrs: {
                  placeholder: `Your program here`,
                  "data-type": `input`,
                },
              },
              state.rawProgramTxt
            ),
          ]),
        ]),
      ]),
    ])
  );
}

const Editor = (sources) => {
  const action$ = intent(sources.DOM);
  const state$ = model(action$);
  const vdom$ = view(state$);
  const scroll$ = xs.merge(
    sources.DOM.select(".robot")
      .events("click")
      .mapTo(sources.DOM.select(".face").element())
      .flatten(),
    sources.DOM.select(".camera")
      .events("click")
      .mapTo(sources.DOM.select(".posenet").element())
      .flatten()
  );
  return {
    state: state$,
    DOM: vdom$,
    ScrollIntoView: scroll$,
  };
};

const scrollIntoViewDriver = (elem$) => {
  elem$.addListener({
    next: (elem) => elem.scrollIntoView(),
  });
};

module.exports = {
  Editor,
  scrollIntoViewDriver,
};
