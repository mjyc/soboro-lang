const xs = require("xstream").default;
const dropRepeats = require("xstream/extra/dropRepeats").default;
const sampleCombine = require("xstream/extra/sampleCombine").default;
const { withState } = require("@cycle/state");
const {
  initializeTabletFaceRobotDrivers,
  withTabletFaceRobotActions,
} = require("@cycle-robot-drivers/run");
const makeVADDriver = require("./makeVADDriver");
const extractPoseFeatures = require("./extractPoseFeatures");

// const ProgramSourceNames = [""];
const ProgramSinkNames = ["setMessage", "askMultipleChoice"];

const Program = (sources) => {
  const sinks = ProgramSinkNames.reduce(
    (prev, name) => {
      if (!prev[name]) prev[name] = xs.never();
      return prev;
    },
    {
      setMessage: sources.isHumanFaceVisible.map((x) => String(x)),
    }
  );
  return sinks;
};

const RobotProgramRunner = (sources) => {
  const intent$ = sources.command;
  const model$ = intent$.fold(
    (prev, intent) => {
      console.log(prev, intent);
      if (prev.status === "ready" && intent.type === "run") {
        // const Program = interpret(intent.value);
        const sinks = Program(sources);
        return {
          status: "running",
          sinks,
        };
      }
      return prev;
    },
    {
      status: "ready",
      sinks: ProgramSinkNames.reduce((prev, name) => {
        prev[name] = xs.never();
        return prev;
      }, {}),
    }
  );
  model$.addListener({ next: console.log });

  const sinks = ProgramSinkNames.reduce((prev, name) => {
    prev[name] = model$.map((model) => model.sinks[name]).flatten();
    return prev;
  }, {});
  return sinks;
};

// const RobotSourceNames = [];
const RobotSinkNames = ["RobotSpeechbubbleAction", "HumanSpeechbubbleAction"];

const Robot = withState(
  withTabletFaceRobotActions((sources) => {
    const settings = {
      startButtonText: "Tap to start",
      samplingFrequencyHz: 2,
    };

    const ready$ = sources.HumanSpeechbubbleAction.result
      .filter((r) => r.result === settings.startButtonText)
      .take(1)
      .mapTo(true)
      .remember();

    const time$ = sources.Time.animationFrames()
      .compose(sources.Time.throttle(1000 / settings.samplingFrequencyHz))
      .map(({ time }) => time);
    const poses$ = time$
      .compose(sampleCombine(sources.PoseDetection.events("poses")))
      .map((x) => x[1]);
    poses$.addListener({ next: () => {} }); // force to start "poses$"
    const isHumanFaceVisible$ = poses$
      .map((poses) => poses.length !== 0)
      .startWith(false)
      .compose(dropRepeats())
      .remember();
    const isHumanSpeaking$ = time$
      .compose(sampleCombine(sources.VAD))
      .map((x) => x[1])
      .startWith(false)
      .compose(dropRepeats())
      .remember();

    const childSources = Object.assign(
      {
        isHumanFaceVisible: isHumanFaceVisible$,
        isHumanSpeaking: isHumanSpeaking$,
      },
      sources
    );
    const childSinks = RobotProgramRunner(childSources);
    const sinks = {
      RobotSpeechbubbleAction: {
        goal: childSinks.setMessage,
      },
      HumanSpeechbubbleAction: {
        goal: xs.merge(
          sources.TabletFace.events("load")
            .take(1)
            .mapTo([settings.startButtonText])
            .remember(),
          childSinks.askMultipleChoice
        ),
      },
    };
    return sinks;
  })
);

const robotDrivers = Object.assign({}, initializeTabletFaceRobotDrivers(), {
  VAD: makeVADDriver(),
});

module.exports = {
  Robot,
  robotDrivers,
};
