const spec = [
  {
    startWhen: "sources.programStarted",
    children: [
      {
        actionName: "say",
        actionValue: "hello",
      },
    ],
  },
  {
    startWhen: "sources.sayFinished == 'hello'",
    endWhen: "sources.sayFinished",
    actionId: "say2",
    actionName: "say",
    actionValue: "nice to meet you",
  },
];

const compile = (spec) => {
  spec.children.map((child) => {
    Object.assign({}, child, {
      startWhen: `${child.startWhen} && ${spec.startWhen}`,
    });
  });
  compile(spec.child);
};

const prog = spec.map(compile);
console.log(prog);

// decorate

const program = {
  type: "main",
  children: [
    {
      type: "component",
      intent: (sources) => {
        sources.programStart.mapTo({
          type: "start",
        });
      },
      model: (intent$) => {
        intent$.fold((prev, intent) => {
          if (intent.type === "start") return "running";
          if (intent.type === "stop") return "ready";
        }, "ready");
      },
      outputs: (model$) => {
        return {
          state: model$.map((x) => model),
          say: model$.mapTo("hello there!"),
        };
      },
    },
    {
      type: "component",
      intent: (sources) => {
        sources.programStart.mapTo({
          type: "start",
        });
      },
      model: (intent$) => {
        intent$.fold((prev, intent) => {
          if (intent.type === "start") return "running";
          if (intent.type === "stop") return "ready";
        }, "ready");
      },
      outputs: (model$) => {
        return {
          state: model$.map((x) => model),
        };
      },
    },
  ],
};

const interpreter = (node) => {
  if (node.type === "main") {
    const children = node.children.map((child) => interpreter(child));
    return (sources) => {
      const sinks = children(sources);
      return sinks;
    };
  }
  if (node.type === "component") {
    return (sources) => {
      const intent$ = node.intent(sources);
      const model$ = intent$.fold(node.model, node.seed);
      const sinks = node.outputs(model$, intent$, sources);
      return sinks;
    };
  }

  const children = [
    (sources) => {
      const input$ = intent$(sources);
      return input$;
    },
    (sources) => {
      const input$ = intent$(sources);
      return input$;
    },
  ];

  return xs.merge(children[0], children[1]);
};

interpreter(program);
