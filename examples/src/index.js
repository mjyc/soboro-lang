const xs = require("xstream").default;
const sampleCombine = require("xstream/extra/sampleCombine").default;
const { div } = require("@cycle/dom");
const { timeDriver } = require("@cycle/time");
const { run } = require("@cycle/run");
const { Editor, scrollIntoViewDriver } = require("./Editor");
const { Robot, robotDrivers } = require("./robot/Robot");

const main = (sources) => {
  document.body.style.margin = 0;

  const editor = Editor(sources);

  const command$ = xs.merge(
    sources.DOM.select(".run")
      .events("click")
      .compose(sampleCombine(editor.state))
      .map(([_, state]) => ({
        type: "run",
        value: state.rawProgramTxt,
      })),
    sources.DOM.select(".stop")
      .events("click")
      .mapTo({
        type: "stop",
      })
  );
  const robotSinks = Robot(Object.assign({ command: command$ }, sources));

  // robotSinks.HumanSpeechbubbleAction.addListener({ next: console.warn });

  vdom$ = xs.combine(editor.DOM, robotSinks.DOM).map((vdoms) => div(vdoms));
  return Object.assign({}, robotSinks, {
    DOM: vdom$,
    ScrollIntoView: editor.ScrollIntoView,
  });
};

const drivers = Object.assign({}, robotDrivers, {
  Time: timeDriver,
  ScrollIntoView: scrollIntoViewDriver,
});

run(main, drivers);
