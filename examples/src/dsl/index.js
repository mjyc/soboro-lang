const xs = require("xstream").default;
const dropRepeats = require("xstream/extra/dropRepeats").default;

// https://stackoverflow.com/a/2117523
const uuidv4 = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    const r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

const createFncFromStr = (fncStr) => {
  return Function(`"use strict";return (${fncStr})`)();
};

/**
 * @param node Program for a robot behavior.
 * @param node.type **Required.** Type of the program, e.g., `"component"`, `"main"`.
 * @param node.sourceNames **Required.** The list of incoming stream names.
 * @param node.sinkNames **Required.** The list of outgoing stream names.
 * @param node.intent **Required.** A string or function representing the componet's "intent" function.
 * @param node.modelReducer **Required.** A string or function representing the componet's "modelReducer" function.
 * @param node.modelSeed **Required.** A string or function representing the componet's "modelSeed" function.
 * @param node.outputs **Required.** A string or function representing the componet's "outputs" function.
 */
const interpret = (node) => {
  if (node.type === "component") {
    const sourceNames = node.sourceNames;
    const intent =
      typeof node.intent === "function"
        ? node.intent
        : createFncFromStr(
            `(xs) => ({ ${sourceNames
              .map((sourceName) => `${sourceName}`)
              .join(", ")} }) => ${node.intent}`
          )(xs);
    const modelReducer =
      typeof node.modelReducer === "function"
        ? node.modelReducer
        : createFncFromStr(`(xs) => (prev, intent) => ${node.modelReducer}`)(
            xs
          );
    const modelSeed = node.modelSeed;
    const outputs =
      typeof node.outputs === "function"
        ? node.outputs
        : createFncFromStr(`(xs) => (model$) => ${node.outputs}`)(xs);
    return (sources) => {
      const intent$ = intent(sources);
      const model$ = intent$
        .fold(modelReducer, modelSeed)
        .compose(dropRepeats()) // converts to "Stream"
        .remember();
      const sinks = outputs(model$);
      // check "Object.keys(field)" against node.sinkNames?
      return sinks;
    };
  }
  if (node.type === "main") {
    const sinkNames = node.children.reduce(
      (prev, child) =>
        prev.concat(
          child.sinkNames.filter((sinkName) => prev.indexOf(sinkName) === -1)
        ),
      []
    );
    const comps = node.children.map(interpret);
    return (sources) => {
      sources.state = node.children.reduce((prev, childNode) => {
        prev[childNode.id] = xs.createWithMemory();
        return prev;
      }, {});
      const compsSinks = comps.map((comp) => comp(sources));
      const sinks = sinkNames.reduce((prev, name) => {
        prev[name] = xs.merge.apply(
          xs,
          compsSinks.map((x) => x[name] || xs.never()) // do this in "component"?
        );
        return prev;
      }, {});
      sinks.state = compsSinks.reduce((prev, compSinks, i) => {
        sources.state[node.children[i].id].imitate(
          compSinks.state.filter(() => true) // "imitate" requires a "Stream"
        );
        prev[node.children[i].id] = compSinks.state;
        return prev;
      }, {});
      return sinks;
    };
  }
  return node;
};

// TODO: update the comments

/**
 * @param spec See "spec" param in "transpile" function
 */
const compile = (spec) => {
  spec = transpile(spec)[0];
  const prog = {
    id: spec.id,
    type: "component",
    // TODO: update "sourceNames" creation
    sourceNames: [spec.starton, "sayResult"],
    sinkNames: Object.keys(spec.run),
    // TODO: update "intent" creation
    intent: `xs.merge(
         ${spec.starton}.filter((x) => Boolean(x)).mapTo("start"),
         sayResult.filter((x) => x === "hello").mapTo("stop")
       )`,
    modelReducer: `prev === "ready" && intent === "start"
          ? "running"
          : prev === "running" && intent === "stop"
          ? "ready"
          : prev`,
    modelSeed: "ready",
    outputs: `({
        state: model$,
        say: model$.filter((x) => x === "running").mapTo("hello"),
      })`,
  };
  return prog;
};

const createPauses = (actionIDs, { condition, run }) => {
  const pauses = actionIDs.map((aid) => ({
    id: `${aid}-pause`,
    starton: `(${condition}) && (actionStatus["${aid}"] === "running")`,
    endon: `!(${condition})`,
    run,
  }));
  return pauses;
};

/**
 * @param spec Specification for a robot behavior. It should have a _[Action]_, _Decorate_, _Concat_, _Sequence_, or _With_ type.
 *
 *
 * #### Action
 *
 * @param spec.run
 * @param spec.run.{actionName}
 *
 *
 * #### Decorate
 *
 * @param spec.decorate An array of _Decorator_s.
 * @param spec.actions An array of _Spec_s.
 *
 *
 * #### Decorator
 * @param decorator
 * @param decorator.filter A function that returns a boolean value.
 * @param decorator.map A function that returns an object value.
 *
 *
 * #### Concat
 * @param spec.concat An array of _Spec_s.
 *
 *
 * #### Sequence
 * @param spec.sequence An array of _Spec_s.
 *
 *
 * #### With
 * @param spec.with An array of _Addon_s.
 * @param spec.actions An array of _Spec_s.
 *
 *
 * #### Addon
 * @param addon
 * @param addon.pause
 * @param addon.pause.condition
 * @param addon.pause.run
 * @param addon.pause.run.{actionName}
 *
 * @returns An array of Action specifications.
 */
const transpile = (spec = {}) => {
  if (spec.with && spec.actions) {
    const actions = transpile(spec.actions);
    const addons = spec.with.map((addon) =>
      addon.pause
        ? createPauses(
            actions.map((a) => a.id),
            addon.pause
          )
        : []
    );
    return transpile({
      concat: [actions, ...addons],
    });
  }
  if (spec.sequence) {
    return transpile({
      decorate: [
        {
          map: (action, index) => ({
            id: `${action.id ? action.id : "seq"}-${index}`,
          }),
        },
        {
          map: (action, index, actions) => ({
            starton:
              index === 0
                ? undefined
                : `actionResultID === "${actions[index - 1].id}"`,
            endon: Object.keys(action.run)
              .map((aname) => `${aname}Result`)
              .join(" && "),
          }),
        },
      ],
      actions: transpile(spec.sequence),
    });
  }
  if (!Array.isArray(spec) && spec.concat) {
    const concat = spec.concat;
    return [].concat(
      ...concat.map((sp) => (typeof sp === "object" ? transpile(sp) : sp))
    );
  }
  if (spec.decorate && spec.actions) {
    const decorate = spec.decorate;
    const actions = transpile(spec.actions);
    return decorate.reduce((prev, decorator, idx) => {
      let cur;
      if (decorator.map) cur = prev.map(decorator.map);
      if (decorator.filter) cur = prev.filter(decorator.filter);
      if (!cur) {
        console.warn(
          `spec.decorate[${idx}].map and spec.decorate[${idx}].filter should NOT be "undefined"`
        );
        cur = prev;
      }
      return cur.map((a, i) => Object.assign(actions[i], a));
    }, actions);
  }
  if (spec.run) {
    return [spec];
  }
  if (Array.isArray(spec) && spec.reduce((prev, sp) => sp.run && prev, true)) {
    return spec;
  }

  console.warn("unexpected input spec; returning the input spec", spec);
  return spec;
};

module.exports = {
  interpret,
  compile,
  transpile,
  createPauses,
};
