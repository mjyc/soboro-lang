

<!-- Start src/dsl/index.js -->

## interpret

### Params:

* *node* Program for a robot behavior.
* *node.type* **Required.** Type of the program, e.g., `"component"`, `"main"`.
* *node.sourceNames* **Required.** The list of incoming stream names.
* *node.sinkNames* **Required.** The list of outgoing stream names.
* *node.intent* **Required.** A string or function representing the componet's "intent" function.
* *node.modelReducer* **Required.** A string or function representing the componet's "modelReducer" function.
* *node.modelSeed* **Required.** A string or function representing the componet's "modelSeed" function.
* *node.outputs* **Required.** A string or function representing the componet's "outputs" function.

## compile

### Params:

* *spec* See "spec" param in "transpile" function

## transpile

### Params:

* *spec* Specification for a robot behavior. It should be _[Action]_, _Decorate_, _Concat_, _Sequence_, or _With_. 
#### Action
* *spec.run* 
* *spec.run.{actionName}* 
#### Decorate
* *spec.decorate* An array of _Decorator_s.
* *spec.actions* An array of _Spec_s. 
#### Decorator
* *decorator* 
* *decorator.filter* A function that returns a boolean value.
* *decorator.map* A function that returns an object value. 
#### Concat
* *spec.concat* An array of _Spec_s. 
#### Sequence
* *spec.sequence* An array of _Spec_s. 
#### With
* *spec.with* An array of _Addon_s.
* *spec.actions* An array of _Spec_s. 
#### Addon
* *addon* 
* *addon.pause* 
* *addon.pause.condition* 
* *addon.pause.run* 
* *addon.pause.run.{actionName}* 

### Return:

* An array of Action specifications.

<!-- End src/dsl/index.js -->

