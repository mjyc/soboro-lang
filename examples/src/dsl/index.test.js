const xs = require("xstream").default;
const { mockTimeSource } = require("@cycle/time");
const { transpile, compile, interpret, createPauses } = require("./index");

describe("interpret", () => {
  it("interprets a main component with mvi functions", (done) => {
    const testProgram = {
      type: "main",
      children: [
        {
          id: "test",
          type: "component",
          sourceNames: ["programStart", "sayResult"],
          sinkNames: ["say"],
          intent: ({ programStart, sayResult }) =>
            xs.merge(
              sources.programStart.filter((x) => Boolean(x)).mapTo("start"),
              sources.sayResult.filter((x) => x === "hello").mapTo("stop")
            ),
          modelReducer: (prev, intent) => {
            if ((prev === "ready", intent === "start")) return "running";
            if ((prev === "running", intent === "stop")) return "ready";
          },
          modelSeed: "ready",
          outputs: (model$) => ({
            state: model$,
            say: model$.filter((x) => x === "running").mapTo("hello"),
          }),
        },
      ],
    };
    const testComponent = interpret(testProgram);

    const Time = mockTimeSource();
    const data = {
      h: "hello",
      r: "ready",
      u: "running",
    };
    const sources = {
      programStart: Time.diagram("-x---|"),
      sayResult: Time.diagram("---h-|", data),
    };
    const expectedSinks = {
      say: Time.diagram("-h---|", data),
      state: {
        test: Time.diagram("ru-r-|", data),
      },
    };
    const actualSinks = testComponent(sources);

    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.assertEqual(actualSinks.state.test, expectedSinks.state.test);
    Time.run(done);
  });

  it("interprets a main component with mvi strings", (done) => {
    const testProgram = {
      type: "main",
      children: [
        {
          id: "test",
          type: "component",
          sourceNames: ["programStart", "sayResult"],
          sinkNames: ["say"],
          intent: `xs.merge(
            programStart.filter((x) => Boolean(x)).mapTo("start"),
            sayResult.filter((x) => x === "hello").mapTo("stop")
          )`,
          modelReducer: `prev === "ready" && intent === "start"
              ? "running"
              : prev === "running" && intent === "stop"
              ? "ready"
              : prev`,
          modelSeed: "ready",
          outputs: `({
            state: model$,
            say: model$.filter((x) => x === "running").mapTo("hello"),
          })`,
        },
      ],
    };
    const testComponent = interpret(testProgram);

    const Time = mockTimeSource();
    const data = {
      h: "hello",
      r: "ready",
      u: "running",
    };
    const sources = {
      programStart: Time.diagram("-x---|"),
      sayResult: Time.diagram("---h-|", data),
    };
    const expectedSinks = {
      say: Time.diagram("-h---|", data),
      state: {
        test: Time.diagram("ru-r-|", data),
      },
    };
    const actualSinks = testComponent(sources);

    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.assertEqual(actualSinks.state.test, expectedSinks.state.test);
    Time.run(done);
  });

  it("interprets a main component with three childrens", (done) => {
    const testProgram = {
      type: "main",
      children: [
        {
          id: "test0",
          type: "component",
          sourceNames: ["programStart", "sayResult"],
          sinkNames: ["say"],
          intent: (sources) => {
            return xs.merge(
              sources.programStart.filter((x) => Boolean(x)).mapTo("start"),
              sources.sayResult.filter((x) => x === "hello").mapTo("stop")
            );
          },
          modelReducer: (prev, intent) => {
            if ((prev === "ready", intent === "start")) return "running";
            if ((prev === "running", intent === "stop")) return "ready";
          },
          modelSeed: "ready",
          outputs: (model$) => {
            return {
              state: model$,
              say: model$.filter((x) => x === "running").mapTo("hello"),
            };
          },
        },
        {
          id: "test1",
          type: "component",
          sourceNames: ["sayResult"],
          sinkNames: ["say"],
          intent: (sources) => {
            return xs.merge(
              sources.state["test0"] // TODO: change to actionResultID
                .filter((x) => x === "ready")
                .drop(1)
                .mapTo("start"),
              sources.sayResult
                .filter((x) => x === "nice to meet you")
                .mapTo("stop")
            );
          },
          modelReducer: (prev, intent) => {
            if ((prev === "ready", intent === "start")) return "running";
            if ((prev === "running", intent === "stop")) return "ready";
          },
          modelSeed: "ready",
          outputs: (model$) => {
            return {
              state: model$,
              say: model$
                .filter((x) => x === "running")
                .mapTo("nice to meet you"),
            };
          },
        },
        {
          id: "test2",
          type: "component",
          sourceNames: ["sayResult"],
          sinkNames: ["say"],
          intent: (sources) => {
            return xs.merge(
              sources.state["test1"] // TODO: change to actionResultID
                .filter((x) => x === "ready")
                .drop(1)
                .mapTo("start"),
              sources.sayResult.filter((x) => x === "bye").mapTo("stop")
            );
          },
          modelReducer: (prev, intent) => {
            if ((prev === "ready", intent === "start")) return "running";
            if ((prev === "running", intent === "stop")) return "ready";
          },
          modelSeed: "ready",
          outputs: (model$) => {
            return {
              state: model$,
              say: model$.filter((x) => x === "running").mapTo("bye"),
            };
          },
        },
      ],
    };
    const testComponent = interpret(testProgram);

    const Time = mockTimeSource();
    const data = {
      h: "hello",
      n: "nice to meet you",
      b: "bye",
      r: "ready",
      u: "running",
    };
    const sources = {
      programStart: Time.diagram("--x---|"),
      sayResult: Time.diagram("---hnb|", data),
    };
    const expectedSinks = {
      say: Time.diagram("--hnb-|", data),
      state: {
        test0: Time.diagram("r-ur--|", data),
        test1: Time.diagram("r--ur-|", data),
        test2: Time.diagram("r---ur|", data),
      },
    };
    const actualSinks = testComponent(sources);

    Time.assertEqual(actualSinks.say, expectedSinks.say);

    Time.assertEqual(actualSinks.state.test0, expectedSinks.state.test0);
    Time.assertEqual(actualSinks.state.test1, expectedSinks.state.test1);
    Time.assertEqual(actualSinks.state.test2, expectedSinks.state.test2);
    Time.run(done);
  });
});

describe("compile", () => {
  it("compiles an action", () => {
    const testSpec = {
      type: "action",
      id: "test",
      starton: "programStart",
      endon: `sayResult === "hello"`,
      run: {
        say: "hello",
      },
    };
    const expectedProgram = {
      id: "test",
      type: "component",
      sourceNames: ["programStart", "sayResult"],
      sinkNames: ["say"],
      intent: `xs.merge(
         programStart.filter((x) => Boolean(x)).mapTo("start"),
         sayResult.filter((x) => x === "hello").mapTo("stop")
       )`,
      modelReducer: `prev === "ready" && intent === "start"
          ? "running"
          : prev === "running" && intent === "stop"
          ? "ready"
          : prev`,
      modelSeed: "ready",
      outputs: `({
        state: model$,
        say: model$.filter((x) => x === "running").mapTo("hello"),
      })`,
    };
    const actualProgram = compile(testSpec);
    expect(actualProgram).toEqual(expectedProgram);
  });
});

describe("createPauses", () => {
  it("creates a pause specifications", () => {
    const actionIDs = ["seq-0", "seq-1", "seq-2"];
    const actualSpec = createPauses(actionIDs, {
      condition: "!isHumanFaceVisible",
      run: { say: null },
    });
    const expectedSpec = [
      {
        id: "seq-0-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-0"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
      {
        id: "seq-1-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-1"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
      {
        id: "seq-2-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-2"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
    ];
    expect(actualSpec).toEqual(expectedSpec);
  });
});

describe("transpile", () => {
  it("transpiles a decorate specification", () => {
    const testSpec = {
      decorate: [{ map: (_, index) => ({ id: `${index}` }) }],
      actions: [{ run: { say: "hello" } }],
    };
    const expectedSpec = [{ id: "0", run: { say: "hello" } }];
    const actualSpec = transpile(testSpec);
    expect(actualSpec).toEqual(expectedSpec);
  });

  it("transpiles a concat specification", () => {
    const testSpec = {
      concat: [
        {
          decorate: [{ map: (_, index) => ({ id: `${index}` }) }],
          actions: [{ run: { say: "hello" } }],
        },
        { id: "1", run: { say: "nice to meet you" } },
        [{ id: "2", run: { say: "bye" } }],
      ],
    };
    const expectedSpec = [
      { id: "0", run: { say: "hello" } },
      { id: "1", run: { say: "nice to meet you" } },
      { id: "2", run: { say: "bye" } },
    ];
    const actualSpec = transpile(testSpec);
    expect(actualSpec).toEqual(expectedSpec);
  });

  it("transpiles a sequence specification", () => {
    const testSpec = {
      sequence: [
        { run: { say: "hello" } },
        { run: { say: "nice to meet you" } },
        { run: { say: "bye" } },
      ],
    };
    const expectedSpec = [
      {
        id: "seq-0",
        starton: undefined,
        endon: "sayResult",
        run: { say: "hello" },
      },
      {
        id: "seq-1",
        starton: 'actionResultID === "seq-0"',
        endon: "sayResult",
        run: { say: "nice to meet you" },
      },
      {
        id: "seq-2",
        starton: 'actionResultID === "seq-1"',
        endon: "sayResult",
        run: { say: "bye" },
      },
    ];
    const actualSpec = transpile(testSpec);
    expect(actualSpec).toEqual(expectedSpec);
  });
});

describe("applications", () => {
  it("authors a storytelling specification", () => {
    const actualSpec = transpile({
      with: [
        {
          pause: {
            condition: "!isHumanFaceVisible",
            run: { say: null },
          },
        },
      ],
      actions: {
        sequence: [
          { run: { say: "hello" } },
          { run: { say: "nice to meet you" } },
          { run: { say: "bye" } },
        ],
      },
    });
    const expectedSpec = [
      {
        id: "seq-0",
        starton: undefined,
        endon: "sayResult",
        run: { say: "hello" },
      },
      {
        id: "seq-1",
        starton: 'actionResultID === "seq-0"',
        endon: "sayResult",
        run: { say: "nice to meet you" },
      },
      {
        id: "seq-2",
        starton: 'actionResultID === "seq-1"',
        endon: "sayResult",
        run: { say: "bye" },
      },
      {
        id: "seq-0-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-0"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
      {
        id: "seq-1-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-1"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
      {
        id: "seq-2-pause",
        starton:
          '(!isHumanFaceVisible) && (actionStatus["seq-2"] === "running")',
        endon: "!(!isHumanFaceVisible)",
        run: { say: null },
      },
    ];
    expect(actualSpec).toEqual(expectedSpec);
  });

  it("authors a q&a specification", () => {
    const actualSpec = transpile({
      flowchart: {
        q0: {
          run: { display: "Which color do you like?", ask: ["red", "blue"] },
          next: {
            q1: `askResult == "red"`,
            q2: `askResult == "blue"`,
          },
        },
        q1: {
          run: { display: "You like red like roses!" },
        },
        q2: {
          run: { display: "You like blue like violets!" },
        },
      },
    });
    const expectedSpec = [];
    // expect(actualSpec).toEqual(expectedSpec);
  });

  it("authors an exercise coach specification", () => {
    const actualSpec = transpile({
      decorate: [
        {
          filter: (action) => ["left-0", "left-1"].indexOf(action.id) !== -1,
          map: (action) => ({
            endon: `(humanFaceDirection == "left") && (actionStatus["${action.id}"] == "running")`,
          }),
        },
        {
          filter: (action) => ["right-0", "right-1"].indexOf(action.id) !== -1,
          map: (action) => ({
            endon: `(humanFaceDirection == "right") && (actionStatus["${action.id}"] == "running")`,
          }),
        },
      ],
      actions: {
        sequence: [
          {
            id: "left-0",
            run: {
              display: "Tilt your head to your left",
            },
          },
          {
            id: "right-0",
            run: {
              display: "Tilt your head to your right",
            },
          },
          {
            id: "left-1",
            run: {
              display: "Tilt your head to your left",
            },
          },
          {
            id: "right-1",
            run: {
              display: "Tilt your head to your right",
            },
          },
        ],
      },
    });
    const expectedSpec = [];
    // expect(actualSpec).toEqual(expectedSpec);
  });
});
